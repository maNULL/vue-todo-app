import axios from 'axios'

export default {
  state: {},
  getters: {},
  mutations: {},
  actions: {
    LOGIN: (commit, payload) => {
      return new Promise((resolve, reject) => {
        axios
          .post('login_check', payload)
          .then(({ data, status }) => {
            if (200 === status) {
              resolve(data)
            }
          })
          .catch(error => {
            reject(error)
          })
      })
    },

    REGISTER: (commit, payload) => {
      return new Promise((resolve, reject) => {
        axios
          .post('register', payload)
          .then(({ data, status }) => {
            if (201 === status) {
              resolve(data)
            }
          })
          .catch(error => {
            reject(error)
          })
      })
    }
  }
}