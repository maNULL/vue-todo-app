import Vue from 'vue'
import VueRouter from 'vue-router'
import './plugins/vuetify'

import axios from 'axios'
import store from './stores/store'

import App from './App.vue'
import Login from './components/Auth/Login'
import SignUp from './components/Auth/SignUp'
import Todo from './components/Todo'
import Tasks from './components/Tasks/Tasks'
import NotesModal from './components/NotesModal'

Vue.config.productionTip = false
Vue.use(VueRouter)

axios.defaults.baseURL = 'http://localhost:8000/api/'
axios.defaults.withCredentials = true

const routes = [
  {
    path: '/login',
    component: Login,
    name: 'login'
  },
  {
    path: '/signup',
    component: SignUp,
    name: 'signup'
  },
  {
    path: '/',
    component: Todo,
    name: 'todo',
    children: [
      {
        path: 'list/:listId',
        components: { tasks: Tasks },
        name: 'tasks',
        children: [
          {
            path: 'task/:taskId',
            components: { notes: NotesModal },
            name: 'notes'
          }
        ]
      }
    ]
  }
]

const router = new VueRouter({
  mode: 'history',
  routes,
  base: '/'
})

new Vue({
  router,
  store,
  render: h => h(App),
}).$mount('#app')
