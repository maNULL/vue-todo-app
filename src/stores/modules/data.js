import axios from 'axios'

export default {
  state: {
    lists: []
  },
  getters: {
    LISTS: state => state.lists,

    TASKS_COUNT: state => index => {
      if (index) {
        return state.lists.find(list => list.id === index).tasks.length
      }
    },

    LIST_TITLE: state => index => {
      if (index) {
        return state.lists.find(list => list.id === index).title
      }
    },

    TASKS: state => index => {
      if (index) {
        return state.lists.find(list => list.id === index).tasks
      }
    },

    NOTES: state => ({ listId, taskId }) => {
      return state.lists
        .find(list => list.id === listId).tasks
        .find(task => task.id === taskId).notes
    },

    TASK_TITLE: state => ({ listId, taskId }) => {
      if (listId && taskId) {
        return state.lists
          .find(list => list.id === listId).tasks
          .find(task => task.id === taskId).title
      }
    }
  },
  mutations: {
    SET_LISTS: (state, payload) => {
      state.lists = payload
    },

    ADD_LIST: (state, payload) => {
      state.lists.unshift(payload)
    },

    SET_TASKS: (state, { data, listId }) => {
      state.lists.find(list => list.id === listId).tasks = data
    },

    ADD_TASK: (state, { data, listId }) => {
      state.lists.find(list => list.id === listId).tasks.push(data)
    },

    SET_TASK_STATUS: (state, { data, taskId, listId }) => {
      state.lists
        .find(list => list.id === listId).tasks
        .find(task => task.id === taskId).isComplete = data
    },

    SET_NOTES: (state, { data, listId, taskId }) => {
      state.lists
        .find(list => list.id === listId).tasks
        .find(task => task.id === taskId).notes = data
    },

    ADD_NOTE: (state, { data, listId, taskId }) => {
      state.lists
        .find(list => list.id === listId).tasks
        .find(task => task.id === taskId).notes.push(data)
    },

    REMOVE_NOTE: (state, { noteId, listId, taskId }) => {
      let notes = state.lists
        .find(list => list.id === listId).tasks
        .find(task => task.id === taskId).notes

      let rs = notes.filter(currentNote => currentNote.id !== noteId)

      state.lists
        .find(list => list.id === listId).tasks
        .find(task => task.id === taskId).notes = [...rs]
    },

    REMOVE_TASK: (state, { listId, taskId }) => {
      let tasks = state.lists
        .find(list => list.id === listId).tasks

      let rs = tasks.filter(currentTask => currentTask.id !== taskId)

      state.lists
        .find(list => list.id === listId).tasks = [...rs]
    },

    UPDATE_TASK_TITLE: (state, { listId, title }) => {
      state.lists.find(list => list.id === listId).title = title
    }

  },
  actions: {
    GET_LISTS: async ({ commit }) => {
      let { data } = await axios.get('lists')
      commit('SET_LISTS', data)
    },

    POST_LIST: ({ commit }, payload) => {
      return new Promise((resolve, reject) => {
        axios
          .post('lists', payload)
          .then(({ data, status }) => {
            commit('ADD_LIST', data)

            if (200 === status) {
              resolve({ data, status })
            }
          })
          .catch(error => reject(error))
      })
    },

    GET_TASKS: async ({ commit }, payload) => {
      let { data } = await axios.get(`lists/${payload}/tasks`)
      commit('SET_TASKS', {
        data,
        listId: payload
      })
    },

    POST_TASK: async ({ commit }, { listId, taskTitle }) => {
      let { data } = await axios.post(`/lists/${listId}/tasks`, {
        title: taskTitle
      })

      commit('ADD_TASK', {
        data,
        listId
      })
    },

    TOGGLE_TASK: async ({ commit }, { listId, taskId }) => {
      let { data } = await axios.patch(`/tasks/${taskId}/status`)

      commit('SET_TASK_STATUS', {
        data,
        taskId,
        listId
      })
    },

    GET_NOTES: async ({ commit }, { listId, taskId }) => {
      let { data } = await axios.get(`tasks/${taskId}/notes`)

      commit('SET_NOTES', {
        data,
        listId,
        taskId
      })
    },

    POST_NOTE: ({ commit }, { note, listId, taskId }) => {
      return new Promise((resolve, reject) => {
        axios
          .post(`tasks/${taskId}/notes`, { note })
          .then(({ data }) => {
            commit('ADD_NOTE', {
              data,
              listId,
              taskId
            })

            resolve(data)
          })
          .catch(error => reject(error))
      })
    },

    DELETE_NOTE: async ({ commit }, { noteId, listId, taskId }) => {
      let { status } = await axios.delete(`notes/${noteId}`)

      if (204 === status) {
        commit('REMOVE_NOTE', { noteId, listId, taskId })
      }
    },

    DELETE_TASK: async ({ commit }, { listId, taskId }) => {
      let { status } = await axios.delete(`tasks/${taskId}`)

      if (204 === status) {
        commit('REMOVE_TASK', { listId, taskId })
      }
    },

    UPDATE_LIST_TITLE: async ({ commit }, { title, listId }) => {
      let { status } = await axios.patch(`lists/${listId}/title`, { title })

      if (204 === status) {
        commit('UPDATE_TASK_TITLE', {
          listId, title
        })
      }
    }

  }
}